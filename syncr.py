#!/usr/bin/env python
"""
example config file (YAML)
---
remote:
  my_remote_host:my_remote_dir/
flags:
  - -a             # archive (rlptgoD)
  - --filter=- .*  # no hidden files
...
"""

import sys
from subprocess import run
from pathlib import Path
from ruamel.yaml import YAML

CONFIG_FILE = '.syncrc'

DEFAULT_FLAGS = [
    f'--filter=- {CONFIG_FILE}'
]

def sync(action, is_verbose=False, is_dryrun=False):

    rc_path = Path(CONFIG_FILE)
    if not rc_path.is_file(): return 1

    rc = YAML().load(rc_path)

    src, dest = {
        'pull': (rc["remote"], '.'), 
        'push': ('.', rc["remote"])
    }[action[0]]

    args = ['rsync'] + DEFAULT_FLAGS + rc.get('flags', []) + [src, dest]
    
    if is_verbose or is_dryrun:
        print(' '.join(args), file=sys.stderr)

    return is_dryrun or run(args).returncode

def main():
    import argparse
    parser = argparse.ArgumentParser(description='rsync wapper tool ' \
                                     f'(reads {CONFIG_FILE} for config)')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-s', '--dry-run', action='store_true')
    parser.add_argument('command', nargs=1, choices=['push', 'pull'])
    args = parser.parse_args()
    sys.exit(sync(args.command, args.verbose, args.dry_run))

if __name__ == '__main__':
    main()
