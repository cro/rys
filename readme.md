# syncr

**syncr** is a simple rsync wrapper utility,
which provides a git-inspired `pull`/`push`
interface on the current working directory.
It reads a `.syncrc` yaml-file to obtain a
remote location and flags for rsync.

Requires `python3` and `ruamel.yaml`.

### Options

- **-v**, **--verbose**: print rsync command before transmission
- **-s**, **--dry-run**: read config and print command without executing rsync

### Sample `.syncrc` file:
```yaml
remote:
  my_remote_host:my_remote_dir
flags:
  - -a              # archive
  - --filter=- .*   # exclude hidden files
  - -c              # skip based on checksum
```
